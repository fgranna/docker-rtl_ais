FROM sysrun/rtlsdr-base:0.1

MAINTAINER Frederik Granna

WORKDIR /tmp

ENV commit_id e262cb526d005cf717a8928d95e80e0779111d0a

RUN git clone https://github.com/dgiardini/rtl-ais.git && \
    cd rtl-ais && \
    git reset --hard $commit_id && \
    make

WORKDIR /tmp/rtl-ais

ENTRYPOINT ["./rtl_ais"]
